import React, {useState, useLayoutEffect} from 'react';
import ReactDOM from 'react-dom';
import SectionMessage from '@atlaskit/section-message';
import Page, {Grid, GridColumn} from '@atlaskit/page';
import Button from '@atlaskit/button';
import styled from 'styled-components';

const ContainerWrapper = styled.div`
  max-width: 780px;
  margin-top: 5%;
  margin-bottom: 5%;
  margin-left: auto;
  margin-right: auto;
  box-shadow: 0 0 10px rgba(0, 0, 0, 0.5);
  display: block;
`;



declare let AP: any;

interface Iprops {
    issueKey : string
}

function ExamplePage(props: Iprops) {
    const [sensitiveInfoFromRequest, setSensitiveInfoFromRequest] = useState("")
    const [issueSummary, setIssueSummary] = useState("")

    useLayoutEffect(() => {
        AP.context.getToken(function(token : string) {

            fetchData(token, `/get-issue-summary`).then((json) => setIssueSummary(json.issueSummary))
        })
    }, );

    async function fetchData(token : string, url : string) {
        const response = await fetch(url, {
            method: 'GET',
            headers: {
                Authorization: `JWT ${token}`,
                'Content-Type': 'application/json',
            },
        });
        return await response.json();;
    }


    const getSensitiveInfo = () => {
        AP.context.getToken(function(token : string) {
            fetchData(token, `/get-sensitive-info`).then((json) => setSensitiveInfoFromRequest(json.sensitiveInfo))
        })
    }

    return (
        <ContainerWrapper>
            <SectionMessage
                title="Repository Information"
            >
                <Page>
                    <Grid>
                        <GridColumn medium={7}>Issue Summary from example-page:</GridColumn>
                        <GridColumn medium={5}><b>{issueSummary}</b></GridColumn>
                    </Grid>
                    <Grid>
                        <GridColumn medium={7}>Sensitive Info from get-sensitive-info:</GridColumn>
                        <GridColumn medium={5}><b>{sensitiveInfoFromRequest}</b></GridColumn>
                    </Grid>

                </Page>
            </SectionMessage>
            <Button appearance="primary" onClick={getSensitiveInfo}>Get sensitive info</Button>
        </ContainerWrapper>
    );
}

window.addEventListener('load', () => {
    const wrapper = document.getElementById('container');
    const issueKey = (document.getElementById('issueKey') as HTMLInputElement).value;
    ReactDOM.render(
        <ExamplePage  issueKey={issueKey}
        />,
        wrapper,
    );
});
