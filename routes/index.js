export default function routes(app, addon) {
    // Redirect root path to /atlassian-connect.json,
    // which will be served by atlassian-connect-express.
    app.get('/', (req, res) => {
        res.redirect('/atlassian-connect.json');
    });

    // This is an example route used by "generalPages" module (see atlassian-connect.json).
    // Verify that the incoming request is authenticated with Atlassian Connect.
    app.get('/hello-world', addon.authenticate(), (req, res) => {
        // Rendering a template is easy; the render method takes two params: the name of the component or template file, and its props.
        // Handlebars and jsx are both supported, but please note that jsx changes require `npm run watch-jsx` in order to be picked up by the server.
        res.render(
          'hello-world.hbs', // change this to 'hello-world.jsx' to use the Atlaskit & React version
          {
            title: 'Atlassian Connect'
            //, issueId: req.query['issueId']
            //, browserOnly: true // you can set this to disable server-side rendering for react views
          }
        );
    });

    app.get('/example-page', addon.authenticate(), (req, res) => {
        // Rendering a template is easy; the render method takes two params: the name of the component or template file, and its props.
        // Handlebars and jsx are both supported, but please note that jsx changes require `npm run watch-jsx` in order to be picked up by the server.
        const {issueKey} = req.query
        res.render(
                'example-page.hbs', // change this to 'hello-world.jsx' to use the Atlaskit & React version
                {
                    title: 'Atlassian Connect',
                    issueKey: issueKey
                    //, issueId: req.query['issueId']
                    //, browserOnly: true // you can set this to disable server-side rendering for react views
                });
    });

    app.get('/get-issue-summary', [addon.authenticate(true), addon.authorizeJira({ project: ["BROWSE_PROJECTS"] })], (req, res) => {
        const issueKey = req.context.context.jira.issue.key
        getIssueSummary(addon, req,issueKey).then((issueSummary) => {
            res.json({issueSummary: issueSummary})
            res.send(200);
        })
    });

    app.get('/get-sensitive-info', [addon.authenticate(true), addon.authorizeJira({ project: ["EDIT_ISSUES"] })], (req, res) => {
        const issueKey = req.context.context.jira.issue.key
        res.json({sensitiveInfo: getSensitiveInfo(issueKey)})
        res.send(200);
    });

    async function getIssueSummary (addon, req, issueKey)  {
        return new Promise((resolve, reject) => {

            var httpClient = addon.httpClient(req);
            httpClient.get(`/rest/api/3/issue/${issueKey}`, function (err, res, body) {
                resolve(JSON.parse(body).fields.summary)
            });
        })
    }

    const getSensitiveInfo = (issueKey) => {
        if (issueKey === "MAT-1"){
            return "sensitive info of MAT-1"
        }
        if (issueKey === "ALEX-1"){
            return "this my super sensitive info for ALEX-1"
        }
        return "no data found"
    }



    // Add additional route handlers here...
}
